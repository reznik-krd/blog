package com.reznik.blog.controllers

import com.reznik.blog.entities.Post
import com.reznik.blog.repository.PostRepository
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam

@Controller
class BlogController(
        private val postRp: PostRepository
) {

    @GetMapping("/blog")
    fun blogMain(model:Model):String {
        model.addAttribute("posts",postRp.findAll())
        return "blog-main"
    }

    @GetMapping("/blog/add")
    fun blogAdd(model:Model):String {
        return "blog-add"
    }

    @PostMapping("/blog/add")
    fun blogPostAdd(@RequestParam title: String, @RequestParam anons: String, @RequestParam fullText: String, model:Model):String {
        postRp.save(Post(title = title, anons = anons, fullText = fullText))
        return "redirect:/blog"
    }

    @GetMapping("/blog/{id}")
    fun blogDetails(@PathVariable(value = "id") id: Long, model: Model): String {

        if(!postRp.existsById(id)) { return("redirect:/blog") }

        val post = postRp.findById(id).get()
        post.views = post.views?.plus(1)
        postRp.save(post)

        val res: ArrayList<Post> = ArrayList()
        res.add(post)

        model.addAttribute("post", res)
        return "blog-details"
    }

    @GetMapping("/blog/{id}/edit")
    fun blogEdit(@PathVariable(value = "id") id: Long, model: Model): String {

        if(!postRp.existsById(id)) { return("redirect:/blog") }

        val res: ArrayList<Post> = ArrayList()
        res.add(postRp.findById(id).get())

        model.addAttribute("post", res)
        return "blog-edit"
    }

    @PostMapping("/blog/{id}/edit")
    fun blogPostUpdate(@PathVariable(value = "id") id: Long,
                       @RequestParam title: String,
                       @RequestParam anons: String,
                       @RequestParam fullText: String,
                       model: Model): String {

        val post = postRp.findById(id).orElseThrow()
        postRp.save(post.apply {
            this.title = title
            this.anons = anons
            this.fullText = fullText
        })
        return "redirect:/blog"
    }

    @PostMapping("/blog/{id}/remove")
    fun blogPostDelete(@PathVariable(value = "id") id: Long, model: Model): String {
        val post = postRp.findById(id).orElseThrow()
        postRp.delete(post)
        return "redirect:/blog"
    }
}