package com.reznik.blog.controllers

import com.reznik.blog.entities.Income
import com.reznik.blog.repository.IncomeRepository
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("income")
class IncomeController(
        private val incomeRp: IncomeRepository
) {

    @GetMapping
    fun list(): MutableList<Income?> = incomeRp.findAll()

    @GetMapping("/{id}")
    fun getOne(@PathVariable("id") id: Long) = incomeRp.findById(id).orElse(null)

    @PostMapping
    fun create(@RequestParam name: String) = incomeRp.save(Income(name = name))

    @DeleteMapping("{id}")
    fun delete(@PathVariable("id") id: Long) {
        incomeRp.deleteById(id)
    }
}