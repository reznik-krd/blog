package com.reznik.blog.repository

import com.reznik.blog.entities.Post
import org.springframework.data.repository.CrudRepository

interface PostRepository : CrudRepository<Post,Long>