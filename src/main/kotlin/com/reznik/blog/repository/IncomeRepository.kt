package com.reznik.blog.repository

import com.reznik.blog.entities.Income
import org.springframework.data.jpa.repository.JpaRepository

interface IncomeRepository : JpaRepository<Income?, Long?>