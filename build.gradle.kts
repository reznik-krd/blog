import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("org.springframework.boot") version "2.2.6.RELEASE"
	id("io.spring.dependency-management") version "1.0.9.RELEASE"
	kotlin("jvm") version "1.3.71"
	kotlin("plugin.spring") version "1.3.71"
}

group = "com.reznik"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
	mavenCentral()
}

dependencies {

	//для работы без интернета, устанавливаем зависимости
	implementation("org.webjars:bootstrap:4.4.1")
	implementation("org.webjars:bootstrap-datepicker:1.9.0")
	implementation("org.webjars:jquery:3.4.1")

	//springframework
	implementation("org.springframework.boot:spring-boot-starter-thymeleaf")
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("org.springframework.boot:spring-boot-devtools")

	//hibernate и BD sqlite
	implementation("org.springframework.boot:spring-boot-starter-data-jpa")
	implementation("com.github.gwenn:sqlite-dialect:0.1.0")
	implementation("org.xerial:sqlite-jdbc:3.30.1")
	implementation("org.hibernate:hibernate-core:5.4.10.Final")

	//kotlin
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

	testImplementation("org.springframework.boot:spring-boot-starter-test") {
		exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "1.8"
	}
}
